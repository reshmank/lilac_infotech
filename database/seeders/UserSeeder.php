<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use app\Models\Department;
use app\Models\Designation;
use app\Models\User;
use Illuminate\Support\Facades\DB;
use Faker\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department=['Sales and Marketing','Application Development','Accounts'];
        $designation=['Marketing Manager','Mobile App Developer','Account'];
        
        $array1=[];
       
        foreach ($department as $key => $value) {

            $array1[$key]['name']=$value;
            // code...
        }

        DB::table('department')->insert($array1);

        $array2=[];

        foreach ($designation as $key => $value) {

            $array2[$key]['name']=$value;
            // code...
        }

        DB::table('designation')->insert($array2);


     for($i=1; $i <=3; $i++)
     {
         $faker = Factory::create();
         $name = $faker->name();
           $insert_data=[
               'name'=>$name,
               'department_id'=>$i,
               'designation_id'=>$i,
 
           ];
         User::create($insert_data);

     }

        

    }
}
