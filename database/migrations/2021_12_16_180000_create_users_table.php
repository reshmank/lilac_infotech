<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Faker\Factory;
use App\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('department_id')->unsigned();
            $table->integer('designation_id')->unsigned();
            $table->timestamps();
        });


     for($i=0; $i < 2; $i++)
     {
         $faker = Factory::create();
         $name = $faker->name();
           $insert_data=[
               'name'=>$name,
               'department_id'=>'2',
               'designation_id'=>'2',
 
           ];
         User::create($insert_data);

     }
     




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
