<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designation', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        // $designation=['Marketing manager','Mobile app developer'];
        // $insert_array=[];
       
        // foreach ($designation as $key => $value) {

        //     $insert_array[$key]['name']=$value;
        //     // code...
        // }

        // DB::table('designation')->insert($insert_array);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designation');
    }
}
