<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Redirect;

class SearchController extends Controller
{

	public function search_data(Request $request)
    {
         $search_data=$request->search;

         if($search_data)
         {
              $search_result=User::select('users.name as username','department.name as department_name','designation.name as designation_name')->join('department','department.id','users.department_id')->join('designation','designation.id','users.designation_id')
               ->orWhereHas('department', function ($query) use ($search_data) {
                $query->where('department.name', 'like', '%'.$search_data.'%');
               })
                ->orWhereHas('designation', function ($query) use ($search_data) {
                $query->where('designation.name', 'like', '%'.$search_data.'%');
               })
              ->orWhere('users.name', 'like', '%' . $search_data . '%')
              ->get()->toArray();

              if($search_result)
              {   return response()->json(['result' => $search_result,'status'=>1]);
              }else
              {
                  return response()->json(['status'=>0]);
              }

         }
      
    }
    

}